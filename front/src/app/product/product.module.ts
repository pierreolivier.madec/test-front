import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { ProductsAdminComponent } from './products-admin/products-admin.component';
import { BrowserModule } from '@angular/platform-browser';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';


@NgModule({
  declarations: [
    ProductsComponent,
    ProductsAdminComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    TableModule,
    InputTextModule,
  ],
  exports: [ProductsAdminComponent, ProductsComponent ]
})

export class ProductModule { }
