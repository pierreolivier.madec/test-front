import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataProductsService } from '../data-products.service';
import { Table } from 'primeng/table';


@Component({
  selector: 'app-products-admin',
  templateUrl: './products-admin.component.html',
  styleUrls: ['./products-admin.component.scss']
})
export class ProductsAdminComponent implements OnInit {
  @ViewChild('dt') table: Table;
  data: any[];
  cols: any[];

  constructor(private dataProductsService: DataProductsService) { }

  jsonData: any;

  ngOnInit(): void {
    this.dataProductsService.getData().subscribe(data => {

      this.jsonData = data;

      console.log(data);

      //this.cols = Object.keys(data[0]).map(key => ({ field: key, header: key }));
    });
  }
  filterTable() {
    this.table.filterGlobal('yourFilterValue', 'contains');
  }
}
